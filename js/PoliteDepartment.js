///////////////////////////////////////////////////////////////////////////////////////////////
////  Vendor Imports
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend quiet "../node_modules/axios/dist/axios.min.js";
// @codekit-prepend quiet "../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";
// @codekit-prepend quiet "../node_modules/@glidejs/glide/dist/glide.min.js";
// @codekit-prepend quiet "../node_modules/validator/validator.min.js";
// @codekit-prepend quiet "../node_modules/animejs/lib/anime.min.js";
// @codekit-prepend quiet "../node_modules/micromodal/dist/micromodal.min.js";
// @codekit-prepend quiet "../node_modules/aos/dist/aos.js";

///////////////////////////////////////////////////////////////////////////////////////////////
////  Polite Department Imports
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend "./modules/_announcement.js";
// @codekit-prepend "./modules/_breakpoints.js";
// @codekit-prepend "./modules/_browser.js";
// @codekit-prepend "./modules/_credits.js";
// @codekit-prepend "./modules/_customer.js";
// @codekit-prepend "./modules/_forms.js";
// @codekit-prepend "./modules/_gliders.js";
// @codekit-prepend "./modules/_header.js";
// @codekit-prepend "./modules/_instagramFeed.js";
// @codekit-prepend "./modules/_mobileMenu.js";
// @codekit-prepend "./modules/_modals.js";
// @codekit-prepend "./modules/_product.js";
// @codekit-prepend "./modules/_scrolling.js";
// @codekit-prepend "./modules/_shopGuide.js";
// @codekit-prepend "./modules/_shopping.js";
// @codekit-prepend "./modules/_sizing.js";
// @codekit-prepend "./modules/_theme.js";
// @codekit-prepend "./modules/_tools.js";

//////////////////////////////////////////////////////////////////////////////////////////
////  Execute Scripts - Immediately!
//////////////////////////////////////////////////////////////////////////////////////////

// ## TODO refactor breakpoints into tools?

let announcement = new Announcement();
let credits = new Credits();
let customer = new Customer();
let forms = new Forms();
let gliders = new Gliders();
let header = new Header();
let instagramFeed = new InstagramFeed();
let mobileMenu = new MobileMenu();
let modals = new Modals();
let product = new Product();
let scrolling = new Scrolling();
let shopGuide = new ShopGuide();
let shopping = new Shopping();
let sizing = new Sizing();

Theme.init([
  sizing,
  announcement,
  credits,
  customer,
  forms,
  gliders,
  header,
  instagramFeed,
  mobileMenu,
  modals,
  product,
  scrolling,
  shopGuide,
  shopping
]);


( document.querySelectorAll('a[data-colour]') || [] ).forEach( link => {

  let colour = link.dataset.colour || false;
  let schema = { container: link.closest('.product-knowledge__schema') || false, cta: false, image: false };
  if ( schema.container ) {
    schema.cta = link.closest('.product-knowledge__schema-cta') || false;
    schema.image = schema.container.querySelector('.product-knowledge__schema-image') || false;
  }

  if ( colour ) {

    // ---------------------------------------- Mouse Over
    link.addEventListener( 'mouseover', ( e ) => {

      link.style.color = '#fff';
      link.style.borderColor = `rgba(${colour}, 1)`;
      link.style.backgroundColor = `rgba(${colour}, 1)`;

      if ( schema.cta ) {
        ( schema.cta.querySelectorAll('.product-knowledge__schema-cta-hr') || [] ).forEach( rule => {
          rule.style.backgroundColor = `rgba(${colour}, 1)`;
        });
      }

      if ( schema.image ) {
        schema.image.style.color = `rgba(${colour}, 1)`;
        ( schema.image.querySelectorAll('.product-knowledge__schema-image-vr') || [] ).forEach( rule => {
          rule.style.backgroundColor = `rgba(${colour}, 1)`;
        });
        ( schema.image.querySelectorAll('.product-knowledge__schema-image-hr') || [] ).forEach( rule => {
          rule.style.backgroundColor = `rgba(${colour}, 1)`;
        });
      }

    });

    // ---------------------------------------- Mouse Out
    link.addEventListener( 'mouseout', ( e ) => {

      link.style.color = '';
      link.style.borderColor = '';
      link.style.backgroundColor = '';

      if ( schema.cta ) {
        ( schema.cta.querySelectorAll('.product-knowledge__schema-cta-hr') || [] ).forEach( rule => {
          rule.style.backgroundColor = '';
        });
      }

      if ( schema.image ) {
        schema.image.style.color = '';
        ( schema.image.querySelectorAll('.product-knowledge__schema-image-vr') || [] ).forEach( rule => {
          rule.style.backgroundColor = '';
        });
        ( schema.image.querySelectorAll('.product-knowledge__schema-image-hr') || [] ).forEach( rule => {
          rule.style.backgroundColor = '';
        });
      }

    });

  }

});

AOS.init({
  //offset: 90,           // BUGGY offset (in px) from the original trigger point
  delay: 0,             // values from 0 to 3000, with step 50ms
  duration: 500,        // values from 0 to 3000, with step 50ms
  easing: 'ease-in-out',     // default easing for AOS animations
  once: false,
  //mirror: true,         // BUGGY
});
