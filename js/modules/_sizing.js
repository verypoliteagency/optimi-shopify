//////////////////////////////////////////////////////////
////  Sizing
//////////////////////////////////////////////////////////

const Sizing = (() => {

  let debug = false;
  let info = { name : 'Sizing', version : '1.0' };

  let tools = new Tools();
  let throttled = false;
  let delay = 250;

  //////////////////////////////////////////////////////////
  ////  Subavigation Hover Events
  //////////////////////////////////////////////////////////

  const setCSSViewportHeight = ( $height ) => {
    tools.setCSSVariable( 'theme-viewport-height', $height + 'px' );
  };

  //////////////////////////////////////////////////////////
  ////  Init
  //////////////////////////////////////////////////////////

  const init = ( $options = false ) => {

    if ( debug ) console.log( `${info.name}.init() v.${info.version} Started` );

    window.addEventListener('resize', function() {
      if (!throttled) {
        setCSSViewportHeight( window.innerHeight );
        throttled = true;
        setTimeout(function() {
          throttled = false;
        }, delay);
      }
    });

    setCSSViewportHeight( window.innerHeight );

    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    init,
  };

});
