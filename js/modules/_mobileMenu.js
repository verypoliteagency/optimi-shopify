//////////////////////////////////////////////////////////
////  Mobile Menu
//////////////////////////////////////////////////////////

const MobileMenu = (() => {

  let debug = false;
  let info = { name : 'MobileMenu', version : '1.0' };

  let tools = new Tools();
  let breakpoints = new Breakpoints();
  let elements = tools.getArrayOfElementsByTag();
  let ticking = false;

  //////////////////////////////////////////////////////////
  ////  Toggle Mobile Menu
  //////////////////////////////////////////////////////////

  const toggleMobileMenu = () => {
    ( document.querySelectorAll('.js--mobile-menu-trigger') || [] ).forEach( trigger => {
      trigger.addEventListener('click',() => {
        tools.toggleClass( 'mobile-menu--active', elements );
      });
    });
  };

  //////////////////////////////////////////////////////////
  ////  Init
  //////////////////////////////////////////////////////////

  const init = ( $options = false ) => {
    if ( debug ) console.log( `${info.name}.init() Started` );

    toggleMobileMenu();

    window.addEventListener('resize', function(e){

      let viewportWidth = window.innerWidth;

      if ( !ticking ) {
        window.requestAnimationFrame(function() {
          if ( viewportWidth >= breakpoints.sizes.lg ) {
            tools.removeClass( 'mobile-menu--active', elements );
          }
          ticking = false;
        });
        ticking = true;
      }
    });

    if ( debug ) console.log( `${info.name}.init() Finished` );
  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    init
  };

});
