//////////////////////////////////////////////////////////
////  Scrolling
//////////////////////////////////////////////////////////

const Scrolling = (() => {

	let debug = false;
	let info = { name : 'Scrolling', version : '1.0' };

  let tools = new Tools();
  let elements = tools.getArrayOfElementsByTag();
  let ticking = false;
  let scrollPosition = {
    current: window.pageYOffset || document.documentElement.scrollTop,
    initial: window.pageYOffset || document.documentElement.scrollTop,
    previous: window.pageYOffset || document.documentElement.scrollTop,
    down: false
  };

  //////////////////////////////////////////////////////////
  ////  Set Scroll State Classes by Scroll Position
  //////////////////////////////////////////////////////////

  const setScrollStateClassesByScrollPosition = ( $scrollPos = 0 ) => {

    // assign scroll direction
    scrollPosition.down = ( scrollPosition.current > scrollPosition.previous ) ? true : false;

    // set scrolling action state
    if ( $scrollPos > 20 ) {
      tools.addClass( 'scroll-pos--scrolled', elements );
    } else {
      tools.removeClass( 'scroll-pos--scrolled', elements );
    }

    // set scroll top position state
    if ( $scrollPos === 0 ) {
      tools.addClass( 'scroll-pos--at-top', elements );
    } else {
      tools.removeClass( 'scroll-pos--at-top', elements );
    }

    // set scroll direction down state
    if ( scrollPosition.down ) {
      tools.addClass( 'scroll-pos--scrolling-down', elements );
    } else {
      tools.removeClass( 'scroll-pos--scrolling-down', elements );
    }

  };

  //////////////////////////////////////////////////////////
  ////  Init
  //////////////////////////////////////////////////////////

  const init = ( $options = false ) => {

    if ( debug ) console.log( `${info.name}.init() Started` );

    setScrollStateClassesByScrollPosition( scrollPosition.current );

    // ---------------------------------------- On resize, throttle but fire setTotalHeaderHeightCSSVariable()
    document.addEventListener( 'scroll', function(e) {

      scrollPosition.previous = scrollPosition.current;
      scrollPosition.current = window.pageYOffset || document.documentElement.scrollTop;

      if ( !ticking ) {
        window.requestAnimationFrame(function() {
          setScrollStateClassesByScrollPosition( scrollPosition.current );
          ticking = false;
        });
        ticking = true;
      }
    });


    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    init
  };

});
