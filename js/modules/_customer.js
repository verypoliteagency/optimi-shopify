//////////////////////////////////////////////////////////
////  Forms
//////////////////////////////////////////////////////////

const Customer = (() => {

  let debug = false;
  let info = { name : 'Customer', version : '1.0' };

  let tools = new Tools();

  //////////////////////////////////////////////////////////
  ////  Toggle New Address Form
  //////////////////////////////////////////////////////////

  const toggleNewAddressForm = () => {
    let newAddressFormElement = document.querySelector('.customers__new-address-form') || false;
    document.querySelectorAll('.js--toggle-new-address-form').forEach( button => {
      button.addEventListener('click', ( event ) => {
        if ( newAddressFormElement ) {
          tools.toggleClass( 'hide', [ newAddressFormElement ] );
        }
      });
    });
  };

  //////////////////////////////////////////////////////////
  ////  Toggle Address Form by ID
  //////////////////////////////////////////////////////////

  const toggleAddressFormByID = () => {
    document.querySelectorAll('.js--toggle-form-by-id').forEach( button => {
      button.addEventListener('click', ( event ) => {
        let targetID = button.getAttribute('data-target-id') || false;
        let targetElement = document.getElementById( targetID ) || false;
        if ( targetElement ) {
          tools.toggleClass( 'hide', [ targetElement ] );
        }
      });
    });
  };

  //////////////////////////////////////////////////////////
  ////  Toggle Recovery Form by ID
  //////////////////////////////////////////////////////////

  const togglePasswordRecoveryForm = () => {
    let loginFormElement = document.getElementById('customers__login-form') || false;
    let passwordRecoveryFormElement = document.getElementById('customers__recover-password-form') || false;
    tools.toggleClass( 'hide', [ loginFormElement, passwordRecoveryFormElement ] );
  }

  //////////////////////////////////////////////////////////
  ////  Delete Address by ID
  //////////////////////////////////////////////////////////

  const deleteAddressByURL = () => {
    document.querySelectorAll('.js--delete-address-by-url').forEach( button => {
      button.addEventListener('click', ( event ) => {
        let targetURL = button.getAttribute('data-url') || false;
        if ( targetURL ) {
          Shopify.postLink( targetURL, {
            parameters: { _method: 'delete' },
          });
        }
      });
    });
  };

  //////////////////////////////////////////////////////////
  ////  Initialize
  //////////////////////////////////////////////////////////

  const init = () => {
    if ( debug ) console.log( `${info.name}.init() v.${info.version} Started` );

    toggleNewAddressForm();
    toggleAddressFormByID();
    deleteAddressByURL();

    document.querySelectorAll('.js--toggle-customer-password-recovery').forEach( button => {
      button.addEventListener('click', ( event ) => {
        togglePasswordRecoveryForm();
      });
    });

    if ( debug ) console.log( `${info.name}.init() Finished` );
  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    info,
    init,
    togglePasswordRecoveryForm
  };

});
