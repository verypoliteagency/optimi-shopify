//////////////////////////////////////////////////////////
////  Product
//////////////////////////////////////////////////////////

const Product = (() => {

  let debug = false;
  let info = { name : 'Product', version : '1.1' };

  let modals = new Modals();

  //////////////////////////////////////////////////////////
  ////  Is Variant Available
  //////////////////////////////////////////////////////////

  const isVariantAvailable = ( $option = false ) => {
    if ( $option && parseInt($option.dataset.inventory) > 0 ) {
      return true;
    }
    return false;
  };

  //////////////////////////////////////////////////////////
  ////  Select on Change
  //////////////////////////////////////////////////////////

  const selectOnChange = () => {
    let select = document.querySelector('.product__variants') || false;
    if ( select ) {
      select.addEventListener('change', function() {
        let option = this.options[this.selectedIndex];
        updateProductFromOption( option );
      });
    }
  };

  //////////////////////////////////////////////////////////
  ////  Select Initial State
  //////////////////////////////////////////////////////////

  const selectInitialState = () => {
    let select = document.querySelector('.product__variants') || false;
    if ( select ) {
      let option = select.options[select.selectedIndex];
      updateProductFromOption( option );
    }
  };

  //////////////////////////////////////////////////////////
  ////  Toggle Inventory Content
  //////////////////////////////////////////////////////////

  const toggleInventoryContent = ( $option = false ) => {

    let inventoryContent = document.querySelector('.product__inventory') || false;
    let isAvailable = isVariantAvailable( $option );

    if ( inventoryContent) {
      if ( isAvailable ) {
        inventoryContent.classList.add('d-none');
      } else {
        inventoryContent.classList.remove('d-none');
      }
    }

  };

  //////////////////////////////////////////////////////////
  ////  Toggle Subscription Details
  //////////////////////////////////////////////////////////

  const toggleSubsciptionDetailsModal = () => {

    let modalID = 'modal--subcription-details';

    document.addEventListener( 'click', ( event ) => {
      if ( event.target.classList.contains('rc_popup__label') ) {
        modals.toggleModalVisibility( modalID, 'show' );
      }
    });

  }

  //////////////////////////////////////////////////////////
  ////  Update Add to Cart Button
  //////////////////////////////////////////////////////////

  const updateAddToCartButton = ( $option = false ) => {
    if ( $option ) {

      let inventory = parseInt($option.dataset.inventory) || 0;
      let inventoryPolicy = $option.dataset.inventoryPolicy || '';

      ( document.querySelectorAll('.product .button--add-product-to-cart') || [] ).forEach( button => {

        if ( inventory > 0 ) {
          button.disabled = false;
          button.innerHTML = 'Add to Bag';
        } else {
          if ( 'continue' == inventoryPolicy ) {
            button.disabled = false;
            button.innerHTML = 'Pre-Order';
          } else {
            button.disabled = true;
            button.innerHTML = 'Out of Stock';
          }
        }

      });

    }
  }

  //////////////////////////////////////////////////////////
  ////  Update Notify Me Form Inputs
  //////////////////////////////////////////////////////////

  const updateNotifyMeFormInputs = ( $option = false ) => {
    if ( $option ) {
      ( document.querySelectorAll('[name="contact[product_variant]"]') || [] ).forEach( input => {
        input.value = $option.text || 'not-set';
      });
      ( document.querySelectorAll('[name="contact[product_variant_sku]"]') || [] ).forEach( input => {
        input.value = $option.dataset.sku || 'not-set';
      });
    }
  };

  //////////////////////////////////////////////////////////
  ////  Update Price
  //////////////////////////////////////////////////////////

  const updatePrice = ( $option = false ) => {
    if ( $option ) {

      let price = parseInt($option.dataset.price) || 0;
      let priceCompare = parseInt($option.dataset.priceCompare) || 0;
      let template = `$${(price/100).toFixed(2)}`;
      if ( priceCompare && priceCompare > price ) {
        template += ` <span class="product__price-compare-at">$${(priceCompare/100).toFixed(2)}</span>`;
      }

      ( document.querySelectorAll('.product__price') || [] ).forEach( element => {
        if ( !element.classList.contains('product__price--recommended') ) {
          element.innerHTML = template;
        }
      });

    }
  }

  //////////////////////////////////////////////////////////
  ////  Update Product From Option
  //////////////////////////////////////////////////////////

  const updateProductFromOption = ( $option = false ) => {
    toggleInventoryContent( $option );
    updateNotifyMeFormInputs( $option );
    updatePrice( $option );
    updateAddToCartButton( $option );
  };

  //////////////////////////////////////////////////////////
  ////  Init
  //////////////////////////////////////////////////////////

  const init = ( $options = false ) => {

    if ( debug ) console.log( `${info.name}.init() v.${info.version} Started` );

    selectInitialState();
    selectOnChange();
    toggleSubsciptionDetailsModal();

    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    init,
  };

});
