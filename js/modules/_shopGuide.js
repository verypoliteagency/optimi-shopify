//////////////////////////////////////////////////////////
////  Shop Guide
//////////////////////////////////////////////////////////

const ShopGuide = (() => {

  const debug = false;
  const info = { name : 'ShopGuide', version : '1.0' };

  //////////////////////////////////////////////////////////
  ////  Set Shop Guide HR Element Width
  //////////////////////////////////////////////////////////

  const setShopGuideElementWidth = () => {

    let shopGuideSections = document.querySelectorAll('.section--shop-guide') || [];

    for( let i = 0; i < shopGuideSections.length; i++ ) {

      let shopGuideItems = shopGuideSections[i].querySelectorAll('.collection.shop-guide') || [];
      let shopGuideElement = shopGuideSections[i].querySelector('.shop-guide__collections-guide') || false;

      if ( shopGuideItems.length > 1 && shopGuideElement ) {

        let elementsToCompare = [];

        for( let j = 0; j < shopGuideItems.length; j++ ) {
          if ( j == 0 || j == (shopGuideItems.length - 1) ) {
            let element = shopGuideItems[j];
            elementsToCompare.push({
              element: element,
              center: element.offsetLeft + element.offsetWidth / 2
            });
          }
        }

        shopGuideElement.style.width = getCenterBetweenElements( elementsToCompare ) + 'px';

      }

    }

  };


  //////////////////////////////////////////////////////////
  ////  Get Element Height by Tag
  //////////////////////////////////////////////////////////

  const getCenterBetweenElements = ( $elements = [] ) => {
    if ( $elements.length == 2 ) {
      return Math.abs($elements[0].center - $elements[1].center);
    }
    return 0;
  };

  //////////////////////////////////////////////////////////
  ////  Init
  //////////////////////////////////////////////////////////

  const init = () => {
    if ( debug ) console.log(`Theme.init() started`);

    let ticking = false;

    setShopGuideElementWidth();

    window.addEventListener('resize', function(e){
      if ( !ticking ) {
        window.requestAnimationFrame(function() {
          setShopGuideElementWidth();
          ticking = false;
        });
        ticking = true;
      }
    });

    if ( debug ) console.log(`Theme.init() finished`);
  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    info,
    init
  };

});
