//////////////////////////////////////////////////////////
////  Header
//////////////////////////////////////////////////////////

const Header = (() => {

  let debug = false;
  let info = { name : 'Header', version : '1.0' };

  let tools = new Tools();
  let mouseLeave = { delay: 500, navigationTimer: false };
  let ticking = false;

  //////////////////////////////////////////////////////////
  ////  Navigation Hover Events
  //////////////////////////////////////////////////////////

  const navigationHoverEvents = () => {

    let links = document.querySelectorAll( '.header .navigation__item.with-children .navigation__link' );

    // ---------------------------------------- On mouseenter links, clear timer and add 'active' class
    links.forEach( element => {
      element.addEventListener('mouseenter', e => {
        let subnavigationElement = document.querySelector( '#subnavigation--' + element.getAttribute('data-navigation-id') ) || false;
        let header = tools.getArrayOfElementsByTag(['header']) || [];
        if ( subnavigationElement ) {
          tools.addClass( 'active', [ subnavigationElement ] );
          tools.addClass( 'subnavigation-active', header );
          clearTimeout( mouseLeave.navigationTimer );
        }
      });
    });

    // ---------------------------------------- On mouseleave links, remove 'active' class
    links.forEach( element => {
      element.addEventListener('mouseleave', e => {
        let subnavigationElement = document.querySelector( '#subnavigation--' + element.getAttribute('data-navigation-id') ) || false;
        let header = tools.getArrayOfElementsByTag(['header']) || [];
        if ( subnavigationElement ) {
          mouseLeave.navigationTimer = setTimeout(function(){
            tools.removeClass( 'active', [ subnavigationElement ] );
            tools.removeClass( 'subnavigation-active', header );
          }, mouseLeave.delay );
        }
      });
    });

  };

  //////////////////////////////////////////////////////////
  ////  Set Total Header Height
  //////////////////////////////////////////////////////

  const setTotalHeaderHeightCSSVariable = () => {
    let headerHeight = tools.getElementHeightByTag('header') || false;
    if ( headerHeight ) tools.setCSSVariable( 'theme-total-header-height', headerHeight + 'px' );
  };

  //////////////////////////////////////////////////////////
  ////  Subavigation Hover Events
  //////////////////////////////////////////////////////////

  const subnavigationHoverEvents = () => {

    let links = document.querySelectorAll( '.header .navigation__item.with-children .subnavigation');

    // ---------------------------------------- On mouseenter subnavigation, clear timer and add 'active' class
    links.forEach( element => {
      element.addEventListener('mouseenter', e => {
        tools.addClass( 'active', [ element ] );
        clearTimeout( mouseLeave.navigationTimer );
      });
    });

    // ---------------------------------------- On mouseleave subnavigation, remove 'active' class
    links.forEach( element => {
      element.addEventListener('mouseleave', e => {
        mouseLeave.navigationTimer = setTimeout(function(){
          tools.removeClass( 'active', [ element ] );
        }, mouseLeave.delay * 0.15 );
      });
    });

  };

  //////////////////////////////////////////////////////////
  ////  Init
  //////////////////////////////////////////////////////////

  const init = ( $options = false ) => {

    if ( debug ) console.log( `${info.name}.init() v.${info.version} Started` );

      setTotalHeaderHeightCSSVariable();
      navigationHoverEvents();
      subnavigationHoverEvents();

      // ---------------------------------------- On resize, throttle but fire setTotalHeaderHeightCSSVariable()
      document.addEventListener( 'resize', function(e) {
        if ( !ticking ) {
          window.requestAnimationFrame(function() {
            setTotalHeaderHeightCSSVariable();
            ticking = false;
          });
          ticking = true;
        }
      });


    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    setTotalHeaderHeightCSSVariable,
    init,
  };

});
