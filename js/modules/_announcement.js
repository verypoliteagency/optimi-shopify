//////////////////////////////////////////////////////////
////  Announcement
//////////////////////////////////////////////////////////

const Announcement = (() => {

  let debug = false;
  let info = { name : 'Announcement', version : '1.0' };

  let breakpoints = new Breakpoints();
  let header = new Header();
  let tools = new Tools();
  let ticking = false;
  let cookie = {
    name: 'optimi_announcement',
    value: 'seen',
    expiresIn: 1,
  };

  //////////////////////////////////////////////////////////
  ////  On Click Handler
  //////////////////////////////////////////////////////////

  const onClickHideAnnouncement = () => {
    document.querySelectorAll( '.js--hide-announcement' ).forEach( button => {
      button.addEventListener( 'click', () => {
        toggleAnnouncementVisibility( 'hide' );
        setAnnouncementCookeie();
      });
    });
  };

  //////////////////////////////////////////////////////////
  ////  Set Announcement Cookie
  //////////////////////////////////////////////////////////

  const setAnnouncementCookeie = () => {
    tools.setCookie( cookie.name, cookie.value, cookie.expiresIn );
  };

  //////////////////////////////////////////////////////////
  ////  Toggle Announcement Visibility
  //////////////////////////////////////////////////////////

  const toggleAnnouncementVisibility = ( $state = 'show' ) => {

    let announcement = document.querySelector( '.announcement' ) || false;

    switch ( $state ) {
      case 'show':
        if ( announcement ) announcement.style.display = 'block';
        break;
      case 'hide':
        if ( announcement ) announcement.style.display = 'none';
        break;
    }

    header.setTotalHeaderHeightCSSVariable();

  };

  //////////////////////////////////////////////////////////
  ////  Show Announcement
  //////////////////////////////////////////////////////////

  const showAnnouncement = () => {

    if ( tools.getCookie( cookie.name, cookie.value ) ) {
      return false;
    }
    return true;

  }

  //////////////////////////////////////////////////////////
  ////  Initialize
  //////////////////////////////////////////////////////////

  const init = () => {

    if ( debug ) console.log( `${info.name}.init() Started` );

    if ( showAnnouncement() ) {
      toggleAnnouncementVisibility( 'show' );
    }

    onClickHideAnnouncement();

    window.addEventListener('resize', function(e){
      if ( !ticking ) {
        window.requestAnimationFrame(function() {

          // execute throttled script
          ticking = false;

        });
        ticking = true;
      }
    });

    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    init
  };

});
