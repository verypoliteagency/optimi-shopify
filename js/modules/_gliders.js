//////////////////////////////////////////////////////////
////  Gliders
//////////////////////////////////////////////////////////

const Gliders = (() => {

  let debug = false;
  let info = { name : 'Gliders', version : '1.0' };

  let tools = new Tools();
  let breakpoints = new Breakpoints();
  let gliders = {};
  let queryTargetElement = '.js--glide';
  let ticking = false;

  //////////////////////////////////////////////////////////
  ////  Get Gliders
  //////////////////////////////////////////////////////////

  const getGliders = () => {
    return document.querySelectorAll( queryTargetElement ) || [];
  };

  //////////////////////////////////////////////////////////
  ////  Glider Options
  //////////////////////////////////////////////////////////

  const gliderOptions = ( $customOptions = false ) => {

    let options = {
      animationDuration: 320,
      autoplay: 5000,
      hoverpause: true,
      perView: 1,
      type: 'carousel',
      rewind: true,
      throttle: 50,
      gap: 0,
    };

    if ( $customOptions && (typeof $customOptions == "object") ) {
      options = { ...options, ...$customOptions };
    }

    return options;

  };

  //////////////////////////////////////////////////////////
  ////  Update Glider Height
  //////////////////////////////////////////////////////////

  const updateGliderHeight = ( $glideContainerSelector = '' ) => {

    if ( debug ) console.log( `updateGliderHeight() Started!` );

    let glideContainer = document.querySelector( $glideContainerSelector );
    let glideActiveElement = glideContainer.querySelector( '.glide__slide--active' );
    let glidetrack = glideContainer.querySelector( '.glide__track' );

    if ( glideActiveElement && glidetrack ) {

      let glideActiveElementHeight = glideActiveElement.offsetHeight;
      let glidetrackHeight = glidetrack.offsetHeight;

      if ( glideActiveElement.querySelector( '.lazyload-item.lazyload-item--image' ) ) {
        glideActiveElement.querySelector( '.lazyload-item.lazyload-item--image' ).style.opacity = 1;
      }

      if ( glidetrackHeight != glideActiveElementHeight ) {
        glidetrack.style.height = (glideActiveElementHeight + 1) + 'px';
      }

    }

    if ( debug ) console.log( `updateGliderHeight() Finished!` );

  };

  //////////////////////////////////////////////////////////
  ////  Update Subnavigation
  //////////////////////////////////////////////////////////

  const updateGliderSubnavigation = ( $gliderObject = false, $index = false ) => {
    if ( $gliderObject ) {
      if ( debug ) console.log( 'updateGliderSubnavigation', $gliderObject );
      let elementSubnavigationItems = document.querySelectorAll( '[data-gallery-id="glide--product-campaign-gallery"]' ) || [];
      if ( elementSubnavigationItems.length ) {
        for (let i = 0; i < elementSubnavigationItems.length; i++) {
          let element = elementSubnavigationItems[i];
          let elementIndex = parseInt( element.getAttribute('data-gallery-item-index') );
          tools.removeClass( 'active', [ element ] );
          if ( $index === elementIndex ) {
            tools.addClass( 'active', [ element ] );
          }
        }
      }

    }
  }

  //////////////////////////////////////////////////////////
  ////  Set Gliders
  //////////////////////////////////////////////////////////

  const setGliders = () => {

    if ( getGliders() ) {
      let gliderElements = getGliders();
      for( let i = 0; i < gliderElements.length; i++ ) {
        let element = gliderElements[i];
        let id = element.id;
        let style = element.getAttribute('data-glide-style') || 'not-set';
        let mobileOnly = element.getAttribute('data-glide-mobile-only') === 'true' || false;
        gliders[id] = {
          glider: false,
          element: element,
          id: id,
          active: false,
          style: style,
          mobileOnly: mobileOnly
        }
      }
    }

  };

  //////////////////////////////////////////////////////////
  ////  Initialize Gliders
  //////////////////////////////////////////////////////////

  const initGliders = () => {

    if ( debug ) console.log( 'initGliders started' );

    let windowWidth = window.innerWidth;
    let options = {};

    for ( let key in gliders ) {

      let glider = gliders[key];
      let gliderStyle = glider.style;

      switch ( gliderStyle ) {
        case 'announcement':
          options = gliderOptions({
            animationDuration: 450,
            autoplay: 2800,
            hoverpause: true,
          });
          break;
        case 'campaign-gallery':
          options = gliderOptions({
            animationDuration: 450,
            autoplay: 2800,
            hoverpause: false,
            peek: 22
          });
          break;
        case 'collection-feature':
          options = gliderOptions({
            breakpoints: {
              // up to 400
              400: {
                gap: 15,
                peek: 85,
              },
              575: {
                gap: 15,
                peek: 95,
              },
              767: {
                // up to 767
                gap: 32,
                peek: 165,
              },
              991: {
                // up to 991
                gap: 32,
                peek: 215,
              }
            }
          });
          break;
        case 'product-campaign-gallery':
          options = gliderOptions({
            animationDuration: 450,
            autoplay: 2800
          });
          break;
        case 'product-gallery':
          options = gliderOptions({
            autoplay: 0
          });
          break;
        case 'product-recommended':
          options = gliderOptions({
            gap: 24,
            perView: 2,
          });
          break;
      }

      if ( glider.mobileOnly ) {
        if ( windowWidth > breakpoints.sizes.lg ) {
          toggleGliderByID( glider.id, false, options );
      	} else {
          toggleGliderByID( glider.id, true, options );
        }
      } else {
        toggleGliderByID( glider.id, true, options );
      }

    }

    if ( debug ) console.log( 'initGliders finished' );

  };

  //////////////////////////////////////////////////////////
  ////  Subnavigation Controls
  //////////////////////////////////////////////////////////

  const subnavigationControls = () => {

    let targets = '.product__campaign-gallery-subnavigation-item';
    let targetElements = document.querySelectorAll('.product__campaign-gallery-subnavigation-item') || [];

    function updateGalleryByIndex( $target = false ){
      let galleryID = $target.getAttribute('data-gallery-id') || false;
      let selectedIndex = $target.getAttribute('data-gallery-item-index') || false;
      let glider = gliders[galleryID];
      if ( glider.active ) {
        glider.glider.go( '=' + selectedIndex );
      }
    }

    targetElements.forEach((target) => {
      target.addEventListener( 'click', function(){
        updateGalleryByIndex( target );
      });
    });

  };

  //////////////////////////////////////////////////////////
  ////  Toggle Glider by ID
  //////////////////////////////////////////////////////////

  const toggleGliderByID = ( $id = false, $enable = true, $options = {} ) => {

    if ( $id ) {

      let glider = gliders[$id];

      if ( $enable ) {
        if ( glider.active === false ) {

          let glide = new Glide( '#' + glider.id, $options );

          glide.on( 'mount.after', function( e ) {
            glider.glider = glide;
            glider.active = true;
            gliders[$id] = glider;
          });

          glide.on([ 'build.after', 'run', 'run.after' ], function( e ) {
            setTimeout( () => updateGliderHeight( glide.selector ), 50 );
          });

          if ( 'product-campaign-gallery' === glider.style ) {
            glide.on( [ 'build.after', 'run' ], function( e ) {
              updateGliderSubnavigation( glider, glide.index );
            });
          }

          glide.mount();

        }
      } else {
        if ( glider.active === true ) {
          glider.glider.destroy();
          glider.element.querySelector( '.glide__track' ).style.height = 'auto';
          glider.active = false;
          gliders[$id] = glider;
        }
      }

    }

  };

  //////////////////////////////////////////////////////////
  ////  Public Method | Initialize
  //////////////////////////////////////////////////////////

  const init = () => {

    if ( debug ) console.log( `${info.name}.init() Started` );

    setGliders();
    initGliders();
    subnavigationControls();

    window.addEventListener('resize', function(e){
      if ( !ticking ) {
        window.requestAnimationFrame(function() {
          initGliders();
          ticking = false;
        });
        ticking = true;
      }
    });

    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    init,
    gliderOptions,
    gliders
  };

});
